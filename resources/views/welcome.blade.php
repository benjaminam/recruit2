<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Assistance rangement</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            span + span {
    margin-left: 115px;
}

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 15px;
                font-size: 15px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .pics {
                padding: 0 55px
            }
            
            .title  {
                margin: 1em 0 0.5em 0;
                color: #343434;
                font-weight: normal;
                font-family: 'Ultra', sans-serif;   
                font-size: 76px;
                line-height: 42px;
                text-transform: uppercase;
                text-shadow: 0 2px white, 0 3px #777;
}

            .m-b-md {
                margin-bottom: 90px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <img src="{{url('/boxes.jpg')}}" alt="Image" height="120" width="120">

            <div class="content">
                <div class="title m-b-md">
                   <strong> Assistance</p>Rangement </strong>
                </div>

                <div class="links ">
                    <!-- <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a> -->
                    <a class="navbar-brand" href="{{ route('clients.index') }}"> 
                        Clients <img src="{{url('/client.jpg')}}" alt="Image" height="70" width="70">
                    </a>
                    <a class="navbar-brand" href="{{ route('workers.index') }} "> 
                    Workers <img src="{{url('/diable.jpg')}}" alt="Image" height="75" width="80">
                    </a>
                    <a class="navbar-brand" href="{{ route('works.index') }}"> 
                    Works <img src="{{url('/transaction.jpg')}}" alt="Image" height="100" width="80">
                    </a>
                </div>
                <!-- <div class="pics">
                <span><img src="{{url('/client.jpg')}}" alt="Image" height="70" width="70"></span>
                <span><img src="{{url('/diable.jpg')}}" alt="Image" height="75" width="80"></span>
                <span><img src="{{url('/transaction.jpg')}}" alt="Image" height="100" width="80"></span>
                </div> -->
            </div>
        </div>
    </body>
</html>
