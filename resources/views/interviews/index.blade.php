@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
<div><a href =  "{{url('/interviews/create')}}" class="text-white btn-lg bg-primary"> Add new Interview</a></div>
<p>
<h1>Interviews details</h1>
<table class = "table table-dark">
    <!-- the table data -->
         <tr>
            <th>Id</th><th>Date</th><th>Interviewer name</th><th>Candidate name</th><th>Summary</th><th>Created</th><th>Updated</th>
        </tr>
        
        @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->user_id}}</td>
            <td>{{$interview->candidate_id}}</td>
            <td>{{$interview->summary}}</td>
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>  
        </tr>   
    @endforeach 
</table>

@endsection
