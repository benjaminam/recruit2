@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
        <h1>Create Interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date">Interview Date</label>
            <input type = "date" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "summary">Interview Summary</label>
            <input type = "text" class="form-control" name = "summary">
        </div> 
        <div class="form-group row">
            <label for="candidate_id" class="col-md-1 col-form-label text-md-right">Candidate</label>
            <div class="col-md-6">
                <select class="form-control" name="candidate_id">                                                                         
                    @foreach ($candidates as $candidate)
                        <option value="{{ $candidate->id }}"> 
                            {{$candidate->name}} 
                        </option>
                    @endforeach  
                </select>
            </div>
        </div> 
        <div class="form-group row">
            <label for="user_id" class="col-md-1 col-form-label text-md-right">Interwiewer</label>
            <div class="col-md-6">
<<<<<<< Updated upstream
                <select class="form-control" name="user_id">  
                <option value="" disabled >{{Auth::user()->name}}</option>                                                                                           
=======
                <select class="form-control" name="user_id">        
                <option value="" disabled >{{Auth::user()->name}}</option>                    
>>>>>>> Stashed changes
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}"> 
                            {{$user->name}} 
                        </option>
                    @endforeach  
                </select>
            </div>
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Create Interview">
        </div>   

        
        

    </form>    
@endsection
