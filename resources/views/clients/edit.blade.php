@extends('layouts.app')

@section('title', 'Edit client')

@section('content')       
       <h1>Edit client</h1>
        <form method = "post" action = "{{action('ClientsController@update',$client->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">client name</label>
            <input type = "text" class="form-control" name = "name" value = {{$client->name}}>
        </div>     
        <div class="form-group">
            <label for = "email">client address</label>
            <input type = "text" class="form-control" name = "address" value = {{$client->address}}>
        </div> 
        <div class="form-group">
            <label for = "email">client email</label>
            <input type = "text" class="form-control" name = "email" value = {{$client->email}}>
        </div> 
        <div class="form-group">
            <label for = "email">client phone</label>
            <input type = "text" class="form-control" name = "phone" value = {{$client->phone}}>
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Update client">
        </div>                       
        </form>    
    </body>
</html>
@endsection
