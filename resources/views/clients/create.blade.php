@extends('layouts.app')

@section('title', 'Create client')

@section('content')
        <h1>Create client</h1>
        <form method = "post" action = "{{action('ClientsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Client name</label>
            <input type = "text" class="form-control" name = "name">
        </div>    
        <div class="form-group">
            <label for = "email">Client address</label>
            <input type = "text" class="form-control" name = "address">
        </div>  
        <div class="form-group">
            <label for = "email">Client email</label>
            <input type = "text" class="form-control" name = "email">
        </div>
        <div class="form-group">
            <label for = "email">Client phone</label>
            <input type = "text" class="form-control" name = "phone">
        </div>  
        <div>
            <input type = "submit" name = "submit" value = "Create client">
        </div>                       
        </form>    
@endsection
