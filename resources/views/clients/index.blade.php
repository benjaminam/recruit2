@extends('layouts.app')

@section('title', 'Clients')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/clients/create')}}" class="text-white btn-lg bg-primary"> Add new client</a></div>
<p>
<h1>List of Clients</h1>
<table class="table table-hover">
    <tr>
        <th>id</th><th>Name</th><th>Address</th><th>Email</th><th>Phone</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
    </tr>
    <!-- the table data -->
    @foreach($clients as $client)
        <tr>
            <td>{{$client->id}}</td>
            <td>{{$client->name}}</td>
            <td>{{$client->address}}</td>
            <td>{{$client->email}}</td>
            <td>{{$client->phone}}</td>
            <td>{{$client->created_at}}</td>
            <td>{{$client->updated_at}}</td>
            <td>
                <a href = "{{route('clients.edit',$client->id)}}"class="text-white btn-lg bg-success">Edit</a>
            </td> 
            <td>
            <a class="text-white btn-lg bg-danger" onclick="return confirm('Are you sure to delete this client?')" href="{{route('clients.delete', $client->id)}}"><i class="fa fa-trash"></i>Delete</a>
            </td>                                                               
        </tr>
    @endforeach
</table>
@endsection

