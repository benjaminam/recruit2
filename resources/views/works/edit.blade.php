@extends('layouts.app')

@section('title', 'Edit Work')

@section('content')       
       <h1>Edit work</h1>
        <form method = "post" action = "{{action('WorksController@update',$work->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "date">Date</label>
            <input type = "date" class="form-control" name = "date" value = {{$work->date}}>
        </div>     
        <div>
    
                  </div>
        <div class="form-group">
            <label for = "address">Work address</label>
            <input type = "text" class="form-control" name = "address" value = {{$work->address}}>
        </div> 

        <div class="form-group">
        <label for = "type">  Housing type</label>
            <div >
                <select class="form-control" name ="type">
                <option value="House">House</option>
                <option value="Appartment">Appartment</option>
                <option value="Enterprise">Enterprise</option>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label for = "numworker">Number of worker</label>
            <input type = "number" class="form-control" name = "numworker" value = {{$work->numworker}}>
        </div> 
        <div class="form-group">
            <label for = "numboxes">Number of boxes</label>
            <input type = "number" class="form-control" name = "numboxes" value = {{$work->numboxes}}>
        </div>
        
        <div class="form-group">
        <label for = "worktype">  Work type</label>
            <div >
                <select class="form-control" name ="worktype">
                <option value="Move in">Move in</option>
                <option value="Move out">Move out</option>
                <option value="Tidy up">Tidy up</option>
                </select>
            </div>

        <div class="form-group">
            <label for = "quote">Quote</label>
            <input type = "number" class="form-control" name = "quote" value = {{$work->quote}}>
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Update work">
        </div>                       
        </form>    
    </body>
</html>
@endsection
