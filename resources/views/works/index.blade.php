@extends('layouts.app')

@section('title', 'Works')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/works/create')}}" class="text-white btn-lg bg-primary"> Add new work</a></div>
<p>
<h1>List of works</h1>
<table class="table table-hover">
<thead >
    <tr>
        <th><b>id</b></th><th>Date</th><th>Client</th><th>Address</th><th>Housing type</th><th>Number of worker</th><th>Number of boxes</th><th>Work type</th><th>Quote</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
    </tr>
    </thead>
    <!-- the table data -->
    @foreach($works as $work)
        <tr>
            <td>{{$work->id}}</td>
            <td>{{$work->date}}</td>
            <td><div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($work->client_id))
                          {{$work->owner->name}}  
                        @else
                          Assign client
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($clients as $client)
                      <a class="dropdown-item" href="{{route('work.changeclient',[$work->id,$client->id])}}">{{$client->name}}</a>
                    @endforeach
                    </div>
                  </div>   </td>
            <td>{{$work->address}}</td>
            <td>{{$work->type}}</td>
            <td>{{$work->numworker}}</td>
            <td>{{$work->numboxes}}</td>
            <td>{{$work->worktype}}</td>
            <td>{{$work->quote}}</td>
            <td>{{$work->created_at}}</td>
            <td>{{$work->updated_at}}</td>
            <td>
                <a href = "{{route('works.edit',$work->id)}}"class="text-white btn-lg bg-success">Edit</a>
            </td> 
            <td>
                    <a class="text-white btn-lg bg-danger" onclick="return confirm('Are you sure to delete this work?')" href="{{route('works.delete', $work->id)}}"><i class="fa fa-trash"></i>Delete</a>

            </td>                                                               
        </tr>
    @endforeach
</table>
@endsection

