@extends('layouts.app')

@section('title', 'users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Delete</th><th>Details</th><th>Authorisation</th><th>makemanager</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>       
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>
               <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>     
            <td>
               <a href = "{{route('users.show',$user->id)}}">Details</a>
               
            </td>   
            <td>
            @foreach ($user->roles as $role)
                        {{ $role->name }}
                  @endforeach
</td>    
            <td>
               <a href = "{{route('users.Manageruser',$user->id)}}">Makemanager</a>
               
            </td>   
            <td>
             <a href = "{{route('userroles.delete',$user->id)}} " >Delete Manager</a>                                                                                 
             </td>
        </tr>

    @endforeach
</table>
@endsection