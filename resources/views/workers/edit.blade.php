@extends('layouts.app')

@section('title', 'Edit Worker')

@section('content')       
       <h1>Edit worker</h1>
        <form method = "post" action = "{{action('WorkersController@update',$worker->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">Worker name</label>
            <input type = "text" class="form-control" name = "name" value = {{$worker->name}}>
        </div>     
        <div class="form-group">
            <label for = "email">Worker address</label>
            <input type = "text" class="form-control" name = "address" value = {{$worker->address}}>
        </div> 
        <div class="form-group">
            <label for = "email">Worker email</label>
            <input type = "text" class="form-control" name = "email" value = {{$worker->email}}>
        </div> 
        <div class="form-group">
            <label for = "email">Worker phone</label>
            <input type = "text" class="form-control" name = "phone" value = {{$worker->phone}}>
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Update worker">
        </div>                       
        </form>    
    </body>
</html>
@endsection
