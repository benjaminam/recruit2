@extends('layouts.app')

@section('title', 'Workers')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/workers/create')}}" class="text-white btn-lg bg-primary"> Add new worker</a></div>
<p>
<h1>List of workers</h1>
<table class="table table-hover">
    <tr>
        <th>id</th><th>Name</th><th>Address</th><th>Email</th><th>Phone</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
    </tr>
    <!-- the table data -->
    @foreach($workers as $worker)
        <tr>
            <td>{{$worker->id}}</td>
            <td>{{$worker->name}}</td>
            <td>{{$worker->address}}</td>
            <td>{{$worker->email}}</td>
            <td>{{$worker->phone}}</td>
            <td>{{$worker->created_at}}</td>
            <td>{{$worker->updated_at}}</td>
            <td>
                <a href = "{{route('workers.edit',$worker->id)}}"class="text-white btn-lg bg-success">Edit</a>
            </td> 
            <td>
            <a class="text-white btn-lg bg-danger" onclick="return confirm('Are you sure to delete this worker?')" href="{{route('workers.delete', $worker->id)}}"><i class="fa fa-trash"></i>Delete</a>
            </td>                                                               
        </tr>
    @endforeach
</table>
@endsection

