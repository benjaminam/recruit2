@extends('layouts.app')

@section('title', 'Create worker')

@section('content')

        <h1>Create worker</h1>
        <form method = "post" action = "{{action('WorkersController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Worker name</label>
            <input type = "text" class="form-control" name = "name">
        </div>    
        <div class="form-group">
            <label for = "email">Worker address</label>
            <input type = "text" class="form-control" name = "address">
        </div>  
        <div class="form-group">
            <label for = "email">Worker email</label>
            <input type = "text" class="form-control" name = "email">
        </div>
        <div class="form-group">
            <label for = "email">Worker phone</label>
            <input type = "text" class="form-control" name = "phone">
        </div>  
        <div>
            <input type = "submit" name = "submit" value = "Create Worker">
        </div>                       
        </form>    
@endsection

