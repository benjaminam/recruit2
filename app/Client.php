<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name','email','address','phone'];
    
    public function clients(){
        return $this->hasMany('app\Client');
    }
}
