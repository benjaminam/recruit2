<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 
use App\Role; 
use App\Department;
use App\Userrole;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;



// full name is "App\Http\Controllers\CandidatesController"; 
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $users = User::all(); 
        $roles = Role::all(); 
        $departments = Department::all();      
        return view('users.index', compact('users','departments','roles'));
    }

     

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidate = new Candidate();
        //$candidate->name = $request->name; 
        //$candidate->email = $request->email;
        $can = $candidate->create($request->all());
        $can->status_id = 1;
        $can->save();
        return redirect('candidates');
    }



    public function changeDepartmentFromUser(Request $request){
                $uid = $request->id;
                $did = $request->department_id;
                $user = User::findOrFail($cid);
                if(Gate::allows('change-department', $user))
                {
                    $from = $user->department->id;
                    if(!Department::allowed($from,$did)) return redirect('users');        
                    $user->department_id = $did;
                    $user->save();
                }else{
                    Session::flash('notallowed', 'You are not allowed to change the departement of the user becuase you are not admin');
                }
                return redirect('users');
                //return back();        
                //$this->changeStatus($cid, $sid);
            }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
                $user = User::findOrFail($id);
                $myuser = Auth::id();
                if(Gate::allows('IsAdmin', $myuser))
                        {
                            return view('users.show1', compact('user'));
                           
                        }else{
                            return view('users.show', compact('user'));
                        }
                        
    
    }

    
        public function MakeManager($uid){
                    if(Auth::user()->isAdmin()){
                        $userrole = new UserRole();
                        
                        $userrole->user_id = $uid;
                        $userrole->role_id = 2;
                        $userrole->save();
                        Session::flash('notallowed','changes succesfully');
                    }
                    return redirect('users');
                
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('add-user');
        $departments = Department::all();
        $user = User::findOrFail($id);
        return view('users.edit', compact('user','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('add-user');
        $user = user::findOrFail($id);
       if(!isset($request->password)){
        $request['password'] = $user->password;   
       }else{
         $request->password = Hash::make($request['password']);   
       } 
       $user->update($request->all());
       return redirect('users');  
    }

    
   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('delete-user');
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users'); 
    }
}