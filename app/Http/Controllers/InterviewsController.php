<?php

namespace App\Http\Controllers;
use App\Candidate;
use App\Interview;
use app\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;


class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
        {     
            $interviews = Interview::all();  
            $candidates = Candidate::all();  
            $users=User::all(); 
            return view('interviews.index', compact('interviews','candidates','users'));
        }

    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        $users = User::all();
      //  if($interviews->isEmpty()){
        //  return redirect()->back()->with('alert', 'You have no interviews!');                    }
        return view('interviews.index', compact('users', 'interviews'));
    }
    
      //  public function showInterviewForm()
    //{
      //  $candidates = Candidate::all();
        //return view('interviews.create', compact('interviews','candidates'));
    //}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function create()
    {
        Gate::authorize('add-interview-manager') || Gate::authorize('add-interview-admin');
        $candidates = Candidate::all();
        $users = User::all();

        return view('interviews.create',compact('candidates','users'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $candidate = new Candidate();
        $int = $interview->create($request->all());
        $int->save();
        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
