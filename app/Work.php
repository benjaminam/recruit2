<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = ['date','address','client_id','type','numworker', 'numboxes','worktype','quote'];

    public function owner(){
        return $this->belongsTo('app\Client','client_id');
    }
}

