<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');
Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');
Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');
Route::get('myinterviews', 'InterviewsController@myInterviews')->name('interview.myinterviews')->middleware('auth');
Route::get('users', 'UsersController@index')->name('users.index')->middleware('auth');
Route::get('users/{id}/delete/', 'UsersController@destroy')->name('user.delete')->middleware('auth');
Route::post('users/changedepartment/', 'UsersController@changeDepartmentFromUser')->name('users.changeDepartmentFromUser')->middleware('auth');
Route::get('users/{id}/show/', 'UsersController@show')->name('users.show')->middleware('auth');
Route::get('users/makemanager/{uid}/', 'UsersController@MakeManager')->name('users.Manageruser');
Route::get('userroles/delete/{id}', 'UserRolesController@destroy')->name('userroles.delete');

Route::resource('interviews', 'InterviewsController')->middleware('auth');


Route::resource('clients', 'ClientsController')->middleware('auth');
Route::get('clients/delete/{id}', 'ClientsController@destroy')->name('clients.delete');

Route::resource('workers', 'WorkersController')->middleware('auth');
Route::get('workers/delete/{id}', 'WorkersController@destroy')->name('workers.delete');



Route::resource('works', 'WorksController')->middleware('auth');
Route::get('works/delete/{id}', 'WorksController@destroy')->name('works.delete');

Route::get('works/changeclient/{wid}/{cid?}', 'WorksController@changeClient')->name('work.changeclient');






Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('/hello', function (){
    return 'Hello Larevel';
}); 

Route::get('/student/{id}', function ($id  = 'No student found'){
    return 'We got student with id '.$id;
});

Route::get('/car/{id?}', function ($id  = null){
    if(isset($id)){
        //TODO: validate for integer  
        return "We got car $id";
    } else {
        return 'We need the id to find your car';
    }
});


Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});

Route::get('/users/{email}/{name?}', function ($email,$name = null) {
    if(!isset($name)){
        $name = 'Mising name';
    }
    return view('users', compact('email','name'));
});




#example
Route::get('/users1/{name?}/{email}', function ($name = null, $email) {
    if(!isset($name)){
        $name = 'Mising name';
    }
    return view('users', compact('email','name'));
});























Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
