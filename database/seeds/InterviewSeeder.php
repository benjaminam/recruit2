<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '2020-07-15',
                'summary' => 'good candidate, know the company and the job is applied for, a bit too young',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date' => '2020-07-15',
                'summary' => 'not good candidate, dont know the company and the job is applied for, no experience, good grades at university',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            ]);            
    }
}
